# factoriser

ES6 Javascript class that finds and stores all prime factors of a given number.

Also stores all known prime numbers in a static property.