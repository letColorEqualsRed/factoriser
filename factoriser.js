class Factoriser {
    constructor(num) {
        this.originalValue = num;
        this.__factorise(num);
    }

    static primes = [2];

    primeFactors = {}

    __factorise(num, tryWith = 2) {
        if(num < 2) return
        while (num > tryWith) {
            if(num % tryWith == 0) {
                this.__addPrimeFactor(tryWith)
                num = num / tryWith
            } else {
                tryWith = this.__getNextPrime(tryWith);
            }    
        }
        this.__addPrimeFactor(num)
        this.__pushNewPrimeIfDoesNotExist(num)
    }

    getOriginalValue = () => this.originalValue

    getFactors(returnAsObject = true) {
        if (returnAsObject) return this.__getFactorsAsObject()
        return this.__getFactorsAsArray()
    }

    __getFactorsAsObject = () => ({...this.primeFactors})

    __getFactorsAsArray() {
        let factors = Object.entries(this.primeFactors)
        factors = factors.map(([key, value]) => {
            let arr = []
            for (let ii = 0; ii < value; ii++) {
                arr.push(parseInt(key))
            }
            return arr
        })
        return [].concat(...factors)
    }

    __getNextPrime(prime) {
        let currIndex = Factoriser.primes.indexOf(prime)
        if (!Factoriser.primes[parseInt(currIndex) + 1]) return this.__findNextPrime(prime)
        return Factoriser.primes[parseInt(currIndex) + 1]
    }

    __isPrime(num) {
        for (let ii = 2; ii <= Math.sqrt(num); ii++) {
            if (num % ii == 0) return false;
        }
        return true
    }

    __findNextPrime(prev) {
        let newPrime = prev + 1
        while (!this.__isPrime(newPrime)) {
            newPrime++
        }
        this.__pushNewPrimeIfDoesNotExist(newPrime)
    
        return newPrime
    }

    __pushNewPrimeIfDoesNotExist(newPrime) {
        if (Factoriser.primes.indexOf(newPrime) < 0) Factoriser.primes.push(newPrime)
    }

    __addPrimeFactor(factor) {
        if (this.primeFactors[factor] === undefined) return this.primeFactors[factor] = 1
        this.primeFactors[factor]++
    }

}

exports.Factoriser = Factoriser